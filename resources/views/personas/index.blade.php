@extends('app')

@section('content') 
    <a class="btn btn-success pull-right" href="{{ url('/personas/create') }}" role="button">Nueva persona</a>
    @include('personas.partials.table')
@endsection