@extends('app')

@section('content')
    <h4 class="text-center">Editar Persona: {{ $persona->nombre  }}</h4>
    {!! Form::model($persona, [ 'route' => ['personas.update', $persona], 'method' => 'PUT']) !!}
        @include('personas.partials.fields')
        <button type="submit" class="btn btn-success btn-block">Guardar cambios</button>
    {!! Form::close() !!}
@endsection