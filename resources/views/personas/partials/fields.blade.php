<div class="form-group">
    {!! Form::label('nombre', 'Nombre', ['for' => 'nombre'] ) !!}
    {!! Form::text('nombre', null , ['class' => 'form-control', 'id' => 'nombre', 'placeholder' => 'Escribe el nombre del pastel...' ]  ) !!}
</div>

<div class="form-group">
    {!! Form::label('apellidos', 'Apellidos', ['for' => 'apellidos'] ) !!}
    {!! Form::text('apellidos', null , ['class' => 'form-control', 'id' => 'apellidos', 'placeholder' => 'Escribe el apellidos de la persona...' ]  ) !!}
</div>