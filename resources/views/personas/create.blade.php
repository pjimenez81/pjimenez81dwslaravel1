@extends('app')

@section('content')
    {!! Form::open([ 'route' => 'personas.store', 'method' => 'POST']) !!}
        @include('personas.partials.fields')
        <button type="submit" class="btn btn-success btn-block">Guardar</button>
    {!! Form::close() !!}
@endsection