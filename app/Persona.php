<?php

namespace Agenda;

use Illuminate\Database\Eloquent\Model;

class Persona extends Model
{
   protected $table = 'personas';
}
