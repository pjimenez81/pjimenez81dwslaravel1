<?php

namespace Agenda\Http\Controllers;

use Illuminate\Http\Request;

class PersonasController extends Controller
{
    public function index() {
        
        $personas = Persona::get();
        return view('personas.index')->with('personas',$personas);
    }
    
    
     public function create() {
        
        return view('personas.create');
    }
    
    public function store(Request $request)
    {
        $persona = new Persona;
        $persona->nombre = $request->input('nombre');
        $persona->apellidos  = $request->input('apellidos');

        $pastel->save();

        return redirect()->route('personas.index');
    }
    
    public function edit($id)
    {
        $persona = Pastel::find($id);
        return view('personas.edit')->with('persona',$persona);
    }
    
    
     public function update(Request $request, $id)
    {
        $persona = Persona::find($id);
        $persona->nombre = $request->input('nombre');
        $persona->apellidos  = $request->input('apellidos');
        $persona->save();
        return redirect()->route('personas.index');
    }
    
    public function destroy($id)
    {
        $pastel = Pastel::find($id);
        $pastel->delete();
        return redirect()->route('pasteles.index');
    }

    // Esta es la segunda opcion
    /*public function destroy($id)
    {
        Persona::destroy($id);
        return redirect()->route('personas.index');
    }*/
}
